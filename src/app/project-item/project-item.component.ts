import { Component, OnInit } from '@angular/core';
declare var $: any;

@Component({
  selector: 'app-project-item',
  templateUrl: './project-item.component.html',
  styleUrls: ['./project-item.component.css']
})
export class ProjectItemComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    $('.openmodale').click(function (e) {
      e.preventDefault();
      $('.modale').addClass('opened');
    });
    $('.closemodale').click(function (e) {
      e.preventDefault();
      $('.modale').removeClass('opened');
    });
  }

}

